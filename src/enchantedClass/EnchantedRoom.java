package enchantedClass;

import abstractClass.AbstractRoom;

public class EnchantedRoom extends AbstractRoom{

	@Override
	public String toString() {
		return "Quarto Encantada";
	}

}
