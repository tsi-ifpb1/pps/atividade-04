package enchantedClass;

import abstractClass.AbstractDoor;

public class EnchantedDoor extends AbstractDoor{
	
	@Override
	public String toString() {
		return "Porta Encantada";
	}

}
