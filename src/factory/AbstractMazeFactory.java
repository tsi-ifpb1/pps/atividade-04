package factory;

import abstractClass.AbstractDoor;
import abstractClass.AbstractMaze;
import abstractClass.AbstractRoom;
import abstractClass.AbstractWall;

public abstract class AbstractMazeFactory {

	public abstract AbstractMaze makeMaze();
	
	public abstract AbstractWall makeWall();
	
	public abstract AbstractRoom makeRoom();
	
	public abstract AbstractDoor makeDoor();
}
