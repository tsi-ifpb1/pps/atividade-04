package factory;

import abstractClass.AbstractDoor;
import abstractClass.AbstractMaze;
import abstractClass.AbstractRoom;
import abstractClass.AbstractWall;
import enchantedClass.EnchantedDoor;
import enchantedClass.EnchantedMaze;
import enchantedClass.EnchantedRoom;
import enchantedClass.EnchantedWall;

public class EnchantedMazeFactory extends AbstractMazeFactory{

	@Override
	public AbstractMaze makeMaze() {
		return new EnchantedMaze();
	}

	@Override
	public AbstractWall makeWall() {
		return new EnchantedWall();
	}

	@Override
	public AbstractRoom makeRoom() {
		return new EnchantedRoom();
	}

	@Override
	public AbstractDoor makeDoor() {
		return new EnchantedDoor();
	}

}
