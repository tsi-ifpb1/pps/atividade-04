package factory;

import abstractClass.AbstractDoor;
import abstractClass.AbstractMaze;
import abstractClass.AbstractRoom;
import abstractClass.AbstractWall;
import normalClass.Door;
import normalClass.Maze;
import normalClass.Room;
import normalClass.Wall;

public class MazeFactory extends AbstractMazeFactory{
	
	@Override
	public AbstractMaze makeMaze() {
		return new Maze();
	}

	@Override
	public AbstractWall makeWall() {
		return new Wall();
	}

	@Override
	public AbstractRoom makeRoom() {
		return new Room();
	}

	@Override
	public AbstractDoor makeDoor() {
		return new Door();
	}

}
